package yash;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PalindromeTest {

	@Test
	void testIsPalindrome() {
		boolean flag = Palindrome.isPalindrome("yash");
		assertTrue(!flag,"It's positive test");
	}
	@Test
	void testIsPalindromeNegative() {
		boolean flag = Palindrome.isPalindrome("yashandgabbar");
		assertFalse(flag,"It's negative test");
	}
	@Test
	void testIsPalindromeBoundryOut() {
		boolean flag = Palindrome.isPalindrome("yash");
		assertFalse(flag,"It's positive test");
	}
	@Test
	void testIsPalindromeBoundryIn() {
		boolean flag = Palindrome.isPalindrome("yash");
		assertTrue(!flag,"It's positive test");
	}

}
